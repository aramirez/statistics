package com.n26.codingchallenge.application.getstatistics;

import com.n26.codingchallenge.domain.TransactionsRepository;

public class GetStatisticsService {
  private final TransactionsRepository repository;

  public GetStatisticsService(TransactionsRepository repository) {
    this.repository = repository;
  }

  public GetStatisticsResponse execute(GetStatisticsRequest request) {
    return GetStatisticsResponse.from(repository.getStatistics());
  }
}
