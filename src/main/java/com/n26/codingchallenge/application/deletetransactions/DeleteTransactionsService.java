package com.n26.codingchallenge.application.deletetransactions;

import com.n26.codingchallenge.domain.TransactionsRepository;

public class DeleteTransactionsService {
  private final TransactionsRepository repository;

  public DeleteTransactionsService(TransactionsRepository repository) {
    this.repository = repository;
  }

  public DeleteTransactionsResponse execute(DeleteTransactionsRequest request) {
    repository.deleteAll();
    return new DeleteTransactionsResponse();
  }
}
