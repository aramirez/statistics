package com.n26.codingchallenge.application.createtransaction;

import java.time.Instant;

public class CreateTransactionRequest {
  private final double amount;
  private final Instant timestamp;

  public CreateTransactionRequest(double amount, Instant timestamp) {
    this.amount = amount;
    this.timestamp = timestamp;
  }

  public double getAmount() {
    return amount;
  }

  public Instant getTimestamp() {
    return timestamp;
  }
}
