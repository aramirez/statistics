package com.n26.codingchallenge.application.createtransaction;

public class CreateTransactionResponse {
  public static final CreateTransactionResponse TRANSACTION_DISCARDED =
      new CreateTransactionResponse(Status.TRANSACTION_DISCARDED);
  public static final CreateTransactionResponse TRANSACTION_PERSISTED =
      new CreateTransactionResponse(Status.TRANSACTION_PERSISTED);
  public static final CreateTransactionResponse TRANSACTION_FAILED =
      new CreateTransactionResponse(Status.TRANSACTION_FAILED);

  private final Status status;

  public enum Status {
    TRANSACTION_DISCARDED,
    TRANSACTION_PERSISTED,
    TRANSACTION_FAILED
  }

  private CreateTransactionResponse(Status status) {
    this.status = status;
  }

  public boolean isPersisted() {
    return status.equals(Status.TRANSACTION_PERSISTED);
  }

  public Status getStatus() {
    return status;
  }
}
