package com.n26.codingchallenge.application.createtransaction;

import com.n26.codingchallenge.domain.TransactionsRepository;
import com.n26.codingchallenge.domain.exceptions.TransactionInFutureException;
import com.n26.codingchallenge.domain.transaction.Transaction;

public class CreateTransactionService {
  private final TransactionsRepository repository;

  public CreateTransactionService(TransactionsRepository repository) {
    this.repository = repository;
  }

  public CreateTransactionResponse execute(CreateTransactionRequest request) {
    try {
      Transaction transaction = new Transaction(request.getAmount(), request.getTimestamp());
      return persistIfNoStale(transaction);
    } catch (TransactionInFutureException error) {
      return CreateTransactionResponse.TRANSACTION_FAILED;
    }
  }

  private CreateTransactionResponse persistIfNoStale(Transaction transaction) {
    if (transaction.isStale()) {
      return CreateTransactionResponse.TRANSACTION_DISCARDED;
    }

    repository.persist(transaction);

    return CreateTransactionResponse.TRANSACTION_PERSISTED;
  }
}
