package com.n26.codingchallenge.domain;

import com.n26.codingchallenge.domain.transaction.Transaction;
import com.n26.codingchallenge.domain.transactionsstatistics.TransactionsStatistics;

public interface TransactionsRepository {
  void persist(Transaction transaction);

  void deleteAll();

  TransactionsStatistics getStatistics();
}
