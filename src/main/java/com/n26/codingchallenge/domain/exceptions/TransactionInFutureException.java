package com.n26.codingchallenge.domain.exceptions;

public class TransactionInFutureException extends RuntimeException {
  public TransactionInFutureException() {
    super("Transactions can not be created in the future.");
  }
}
