package com.n26.codingchallenge.domain;

import java.time.Instant;

public class Timestamp {
  private static final int STALENESS_THRESHOLD_IN_SECONDS = 60;

  private final Instant timestamp;

  public Timestamp(Instant timestamp) {
    this.timestamp = timestamp;
  }

  public boolean isStale() {
    return timestamp.plusSeconds(STALENESS_THRESHOLD_IN_SECONDS).isBefore(Instant.now());
  }

  public Instant getValue() {
    return timestamp;
  }
}
