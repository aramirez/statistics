package com.n26.codingchallenge.domain.transactionsstatistics;

import java.math.BigDecimal;
import java.util.Objects;

abstract class Number {
  protected final BigDecimal amount;

  Number(double amount) {
    this.amount = BigDecimal.valueOf(amount).setScale(2, BigDecimal.ROUND_HALF_UP);
  }

  double getValue() {
    return amount.doubleValue();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    return getValue() == ((Number) obj).getValue();
  }

  @Override
  public int hashCode() {
    return Objects.hash(amount.doubleValue());
  }
}
