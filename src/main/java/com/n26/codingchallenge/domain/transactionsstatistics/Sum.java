package com.n26.codingchallenge.domain.transactionsstatistics;

class Sum extends Number {
  Sum(double amount) {
    super(amount);
  }

  Sum merge(Sum sum) {
    return new Sum(amount.add(sum.amount).doubleValue());
  }
}
