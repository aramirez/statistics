package com.n26.codingchallenge.domain.transactionsstatistics;

class Max extends Number {
  Max(double amount) {
    super(amount);
  }

  Max merge(Max other) {
    return new Max(Math.max(getValue(), other.getValue()));
  }
}
