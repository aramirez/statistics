package com.n26.codingchallenge.domain.transactionsstatistics;

import com.n26.codingchallenge.domain.transaction.Transaction;

public class TransactionsStatistics {
  private final Sum sum;
  private final Max max;
  private final Min min;
  private final Count count;

  public TransactionsStatistics(double sum, double max, double min, long count) {
    this.sum = new Sum(sum);
    this.max = new Max(max);
    this.min = new Min(min);
    this.count = new Count(count);
  }

  private TransactionsStatistics(Sum sum, Max max, Min min, Count count) {
    this(sum.getValue(), max.getValue(), min.getValue(), count.getValue());
  }

  public static TransactionsStatistics from(Transaction transaction) {
    return new TransactionsStatistics(
        transaction.getAmount(),
        transaction.getAmount(),
        transaction.getAmount(),
        1L
    );
  }

  public double getSum() {
    return sum.getValue();
  }

  public double getAvg() {
    if (getCount() == 0) {
      return 0d;
    }
    return new Avg(getSum() / getCount()).getValue();
  }

  public double getMax() {
    return max.getValue();
  }

  public double getMin() {
    return min.getValue();
  }

  public long getCount() {
    return count.getValue();
  }

  public TransactionsStatistics merge(TransactionsStatistics other) {
    return new TransactionsStatistics(
        sum.merge(other.sum),
        max.merge(other.max),
        min.merge(other.min),
        count.merge(other.count)
    );
  }
}
