package com.n26.codingchallenge.domain.transactionsstatistics;

class Min extends Number {
  Min(double min) {
    super(min);
  }

  Min merge(Min other) {
    return new Min(Math.min(getValue(), other.getValue()));
  }
}
