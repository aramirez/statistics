package com.n26.codingchallenge.domain.transactionsstatistics;

import java.util.Objects;

class Count {
  private final long count;

  Count(long count) {
    this.count = count;
  }

  public long getValue() {
    return count;
  }

  Count merge(Count other) {
    return new Count(getValue() + other.getValue());
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Count count1 = (Count) obj;
    return count == count1.count;
  }

  @Override
  public int hashCode() {
    return Objects.hash(count);
  }
}
