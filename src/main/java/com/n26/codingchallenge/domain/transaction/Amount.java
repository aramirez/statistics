package com.n26.codingchallenge.domain.transaction;

import java.math.BigDecimal;

class Amount {
  private final BigDecimal amount;

  Amount(double amount) {
    this.amount = BigDecimal.valueOf(amount);
  }

  double getValue() {
    return amount.doubleValue();
  }
}
