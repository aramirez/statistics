package com.n26.codingchallenge.domain.transaction;

import com.n26.codingchallenge.domain.Timestamp;
import com.n26.codingchallenge.domain.exceptions.TransactionInFutureException;

import java.time.Instant;
import java.util.Objects;

public class Transaction {
  private final Amount amount;
  private final Timestamp timestamp;

  public Transaction(double amount, Instant timestamp) {
    requireNoFutureTimestamp(timestamp);
    this.amount = new Amount(amount);
    this.timestamp = new Timestamp(timestamp);
  }

  private void requireNoFutureTimestamp(Instant timestamp) {
    if (timestamp.isAfter(Instant.now())) {
      throw new TransactionInFutureException();
    }
  }

  public boolean isStale() {
    return timestamp.isStale();
  }

  public double getAmount() {
    return amount.getValue();
  }

  public Instant getTimestamp() {
    return timestamp.getValue();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    Transaction that = (Transaction) obj;
    return getAmount() == that.getAmount() && getTimestamp().equals(that.getTimestamp());
  }

  @Override
  public int hashCode() {
    return Objects.hash(amount, timestamp);
  }
}
