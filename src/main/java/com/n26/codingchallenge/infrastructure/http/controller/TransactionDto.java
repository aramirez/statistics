package com.n26.codingchallenge.infrastructure.http.controller;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

class TransactionDto {
  private final double amount;
  private final Instant timestamp;

  TransactionDto(
      @JsonProperty(value = "amount", required = true) double amount,
      @JsonProperty(value = "timestamp", required = true) Instant timestamp
  ) {
    this.amount = amount;
    this.timestamp = timestamp;
  }

  double getAmount() {
    return amount;
  }

  Instant getTimestamp() {
    return timestamp;
  }
}
