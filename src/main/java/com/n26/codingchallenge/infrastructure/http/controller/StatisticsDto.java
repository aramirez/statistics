package com.n26.codingchallenge.infrastructure.http.controller;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.n26.codingchallenge.application.getstatistics.GetStatisticsResponse;

import java.math.BigDecimal;

class StatisticsDto {
  private final double sum;
  private final double avg;
  private final double max;
  private final double min;
  private final long count;

  StatisticsDto(double sum, double avg, double max, double min, long count) {
    this.sum = sum;
    this.avg = avg;
    this.max = max;
    this.min = min;
    this.count = count;
  }

  static StatisticsDto fromUseCaseResponse(GetStatisticsResponse response) {
    return new StatisticsDto(
        response.getSum(),
        response.getAvg(),
        response.getMax(),
        response.getMin(),
        response.getCount()
    );
  }

  @JsonProperty("sum")
  @JsonFormat(shape = JsonFormat.Shape.STRING)
  @JsonSerialize(using = NumberSerializer.class)
  BigDecimal getSum() {
    return BigDecimal.valueOf(sum);
  }

  @JsonProperty("avg")
  @JsonFormat(shape = JsonFormat.Shape.STRING)
  @JsonSerialize(using = NumberSerializer.class)
  BigDecimal getAvg() {
    return BigDecimal.valueOf(avg);
  }

  @JsonProperty("max")
  @JsonFormat(shape = JsonFormat.Shape.STRING)
  @JsonSerialize(using = NumberSerializer.class)
  BigDecimal getMax() {
    return BigDecimal.valueOf(max);
  }

  @JsonProperty("min")
  @JsonFormat(shape = JsonFormat.Shape.STRING)
  @JsonSerialize(using = NumberSerializer.class)
  BigDecimal getMin() {
    return BigDecimal.valueOf(min);
  }

  @JsonProperty("count")
  long getCount() {
    return count;
  }
}
