package com.n26.codingchallenge.infrastructure.http.controller;

import com.n26.codingchallenge.application.createtransaction.CreateTransactionRequest;
import com.n26.codingchallenge.application.createtransaction.CreateTransactionResponse;
import com.n26.codingchallenge.application.createtransaction.CreateTransactionService;
import com.n26.codingchallenge.application.deletetransactions.DeleteTransactionsRequest;
import com.n26.codingchallenge.application.deletetransactions.DeleteTransactionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionsController {

  private final CreateTransactionService createTransactionService;
  private final DeleteTransactionsService deleteTransactionsService;

  @Autowired
  public TransactionsController(
      CreateTransactionService createTransactionService,
      DeleteTransactionsService deleteTransactionsService
  ) {
    this.createTransactionService = createTransactionService;
    this.deleteTransactionsService = deleteTransactionsService;
  }

  @RequestMapping(
      value = "/transactions",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
  )
  public ResponseEntity<Void> createTransaction(@RequestBody TransactionDto transactionDto) {
    CreateTransactionRequest request = new CreateTransactionRequest(
        transactionDto.getAmount(),
        transactionDto.getTimestamp()
    );
    CreateTransactionResponse response = createTransactionService.execute(request);

    switch (response.getStatus()) {
      case TRANSACTION_PERSISTED:
        return new ResponseEntity<>(HttpStatus.CREATED);
      case TRANSACTION_DISCARDED:
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      case TRANSACTION_FAILED:
        return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
      default:
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
  }

  @RequestMapping(value = "/transactions", method = RequestMethod.DELETE)
  public ResponseEntity<Void> deleteTransactions() {
    deleteTransactionsService.execute(new DeleteTransactionsRequest());
    return ResponseEntity.noContent().build();
  }
}
