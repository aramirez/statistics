package com.n26.codingchallenge.infrastructure.http.controller;

import com.n26.codingchallenge.application.getstatistics.GetStatisticsRequest;
import com.n26.codingchallenge.application.getstatistics.GetStatisticsResponse;
import com.n26.codingchallenge.application.getstatistics.GetStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatisticsController {
  private final GetStatisticsService getStatisticsService;

  @Autowired
  public StatisticsController(GetStatisticsService getStatisticsService) {
    this.getStatisticsService = getStatisticsService;
  }

  @RequestMapping(
      value = "/statistics",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE
  )
  public ResponseEntity<StatisticsDto> getStatistics() {
    GetStatisticsResponse response = getStatisticsService.execute(new GetStatisticsRequest());
    return ResponseEntity.ok(StatisticsDto.fromUseCaseResponse(response));
  }
}
