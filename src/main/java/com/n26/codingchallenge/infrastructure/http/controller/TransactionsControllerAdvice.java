package com.n26.codingchallenge.infrastructure.http.controller;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class TransactionsControllerAdvice extends ResponseEntityExceptionHandler {
  @Override
  protected final ResponseEntity<Object> handleHttpMessageNotReadable(
      HttpMessageNotReadableException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request
  ) {
    if (ex.getCause() instanceof InvalidFormatException) {
      return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
    }
    return new ResponseEntity(HttpStatus.BAD_REQUEST);
  }
}
