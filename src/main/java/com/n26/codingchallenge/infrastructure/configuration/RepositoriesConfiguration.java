package com.n26.codingchallenge.infrastructure.configuration;

import com.n26.codingchallenge.domain.TransactionsRepository;
import com.n26.codingchallenge.infrastructure.repositories.InMemoryTransactionsRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RepositoriesConfiguration {
  @Bean
  TransactionsRepository transactionsRepository() {
    return new InMemoryTransactionsRepository();
  }
}
