package com.n26.codingchallenge.infrastructure.configuration;

import com.n26.codingchallenge.application.createtransaction.CreateTransactionService;
import com.n26.codingchallenge.application.deletetransactions.DeleteTransactionsService;
import com.n26.codingchallenge.application.getstatistics.GetStatisticsService;
import com.n26.codingchallenge.domain.TransactionsRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCasesConfiguration {
  @Bean
  CreateTransactionService createTransactionService(TransactionsRepository repository) {
    return new CreateTransactionService(repository);
  }

  @Bean
  DeleteTransactionsService deleteTransactionsService(TransactionsRepository repository) {
    return new DeleteTransactionsService(repository);
  }

  @Bean
  GetStatisticsService getStatisticsService(TransactionsRepository repository) {
    return new GetStatisticsService(repository);
  }
}
