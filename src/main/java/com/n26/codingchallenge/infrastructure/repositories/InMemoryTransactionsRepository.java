package com.n26.codingchallenge.infrastructure.repositories;

import com.n26.codingchallenge.domain.Timestamp;
import com.n26.codingchallenge.domain.TransactionsRepository;
import com.n26.codingchallenge.domain.transaction.Transaction;
import com.n26.codingchallenge.domain.transactionsstatistics.TransactionsStatistics;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryTransactionsRepository implements TransactionsRepository {

  private ConcurrentHashMap<Key, TransactionsStatistics> storage = new ConcurrentHashMap<>();

  @Override
  public void persist(Transaction transaction) {
    doPersistTransaction(transaction);
    deleteStaleTransactions();
  }

  private void doPersistTransaction(Transaction transaction) {
    storage.compute(
        Key.from(transaction.getTimestamp()),
        (key, actual) -> (actual != null)
            ? actual.merge(TransactionsStatistics.from(transaction))
            : TransactionsStatistics.from(transaction)
    );
  }

  private void deleteStaleTransactions() {
    storage.keySet().removeIf(key -> new Timestamp(key.getValue()).isStale());
  }

  @Override
  public void deleteAll() {
    storage.clear();
  }

  @Override
  public TransactionsStatistics getStatistics() {
    return storage.entrySet().stream()
        .filter(entrySet -> !new Timestamp(entrySet.getKey().getValue()).isStale())
        .map(Map.Entry::getValue)
        .reduce(TransactionsStatistics::merge)
        .orElse(new TransactionsStatistics(0d, 0d, 0d, 0L));
  }

  private static class Key {
    private final Instant timestamp;

    private Key(Instant timestamp) {
      this.timestamp = timestamp;
    }

    public Instant getValue() {
      return timestamp;
    }

    static Key from(Instant timestamp) {
      return new Key(timestamp.truncatedTo(ChronoUnit.SECONDS));
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null || getClass() != obj.getClass()) {
        return false;
      }
      Key key = (Key) obj;
      return timestamp.equals(key.timestamp);
    }

    @Override
    public int hashCode() {
      return Objects.hash(timestamp);
    }
  }
}
