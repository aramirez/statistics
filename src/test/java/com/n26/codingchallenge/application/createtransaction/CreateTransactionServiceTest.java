package com.n26.codingchallenge.application.createtransaction;

import com.n26.codingchallenge.domain.TransactionsRepository;
import com.n26.codingchallenge.helpers.TransactionBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CreateTransactionServiceTest {

  private static final CreateTransactionRequest VALID_TRANSACTION_REQUEST =
      new CreateTransactionRequest(12.3343, TransactionBuilder.valid().getTimestamp());
  private static final CreateTransactionRequest STALE_TRANSACTION_REQUEST =
      new CreateTransactionRequest(12.3343, TransactionBuilder.stale().getTimestamp());
  private static final CreateTransactionRequest FUTURE_TRANSACTION_REQUEST =
      new CreateTransactionRequest(12.3343, TransactionBuilder.valid().getTimestamp().plus(60, ChronoUnit.DAYS));

  private CreateTransactionService createTransactionService;

  @Mock
  private TransactionsRepository repository;

  @Before
  public void setUp() {
    createTransactionService = new CreateTransactionService(repository);
  }

  @Test
  public void it_should_persist_a_transaction() {
    createTransactionService.execute(VALID_TRANSACTION_REQUEST);

    verify(repository).persist(argThat(transaction ->
        transaction.getTimestamp().equals(VALID_TRANSACTION_REQUEST.getTimestamp())
        && transaction.getAmount() == VALID_TRANSACTION_REQUEST.getAmount()
    ));
  }

  @Test
  public void it_should_return_message_with_transaction_persisted() {
    CreateTransactionResponse response = createTransactionService.execute(VALID_TRANSACTION_REQUEST);

    assertThat(response.isPersisted()).isTrue();
  }

  @Test
  public void it_should_discard_a_stale_transaction() {
    createTransactionService.execute(STALE_TRANSACTION_REQUEST);

    verify(repository, never()).persist(any());
  }

  @Test
  public void it_should_return_message_with_transaction_discarded() {
    CreateTransactionResponse response = createTransactionService.execute(STALE_TRANSACTION_REQUEST);

    assertThat(response.isPersisted()).isFalse();
  }

  @Test
  public void it_should_not_persist_transactions_in_the_future() {
    CreateTransactionResponse response = createTransactionService.execute(FUTURE_TRANSACTION_REQUEST);
    assertThat(response.isPersisted()).isFalse();
  }
}
