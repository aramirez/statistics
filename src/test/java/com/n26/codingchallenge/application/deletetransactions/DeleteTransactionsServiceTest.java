package com.n26.codingchallenge.application.deletetransactions;

import com.n26.codingchallenge.domain.TransactionsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DeleteTransactionsServiceTest {

  @Mock
  private TransactionsRepository repository;

  @Test
  public void it_should_delete_all_transactions() {
    DeleteTransactionsService deleteTransactionsService = new DeleteTransactionsService(repository);
    deleteTransactionsService.execute(new DeleteTransactionsRequest());

    verify(repository).deleteAll();
  }
}
