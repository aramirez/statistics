package com.n26.codingchallenge.application.getstatistics;

import com.n26.codingchallenge.domain.TransactionsRepository;
import com.n26.codingchallenge.domain.transactionsstatistics.TransactionsStatistics;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetStatisticsServiceTest {

  private static final double SUM = 1000.10;
  private static final double MAX = 200000.5;
  private static final double MIN = 50;
  private static final int COUNT = 10;

  @Mock
  private TransactionsRepository repository;

  @Test
  public void it_should_get_statistics_from_the_repository() {
    TransactionsStatistics statistics = new TransactionsStatistics(SUM, MAX, MIN, COUNT);
    when(repository.getStatistics()).thenReturn(statistics);

    GetStatisticsService getStatisticsService = new GetStatisticsService(repository);
    GetStatisticsResponse response = getStatisticsService.execute(new GetStatisticsRequest());

    assertThat(response.getSum()).isEqualTo(SUM);
    assertThat(response.getCount()).isEqualTo(COUNT);
  }
}
