package com.n26.codingchallenge.domain;

import com.n26.codingchallenge.domain.transaction.Transaction;
import com.n26.codingchallenge.domain.exceptions.TransactionInFutureException;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class TransactionTest {
  @Test
  public void it_should_return_true_transaction_is_stale() {
    Transaction staleTransaction = new Transaction(12.3343, Instant.parse("2018-07-17T09:59:51.312Z"));

    assertThat(staleTransaction.isStale())
        .as("Transaction should be stale because is older than 60 secs.")
        .isTrue();
  }

  @Test
  public void it_should_return_false_transaction_is_not_stale() {
    Transaction staleTransaction = new Transaction(12.3343, Instant.now().minusSeconds(10));

    assertThat(staleTransaction.isStale())
        .as("Transaction should not be stale because is not older than 60 secs.")
        .isFalse();
  }

  @Test
  public void it_should_throw_an_exception_if_timestamp_is_in_the_future() {
    assertThatThrownBy(() -> new Transaction(12.3343, Instant.now().plus(10, ChronoUnit.DAYS)))
        .isInstanceOf(TransactionInFutureException.class);
  }

  @Test
  public void it_should_be_equals_if_amount_and_timestamp_are_the_same() {
    int amount = 12;
    Instant timestamp = Instant.now();
    assertThat(new Transaction(amount, timestamp)).isEqualTo(new Transaction(amount, timestamp));
  }

  @Test
  public void it_should_not_be_equals_if_either_amount_and_timestamp_are_different() {
    Instant timestamp = Instant.now();
    assertThat(new Transaction(12, timestamp)).isNotEqualTo(new Transaction(10, timestamp));
    assertThat(new Transaction(10, timestamp)).isNotEqualTo(new Transaction(10, timestamp.minusSeconds(60)));
  }
}
