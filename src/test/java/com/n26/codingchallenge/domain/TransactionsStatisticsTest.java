package com.n26.codingchallenge.domain;

import com.n26.codingchallenge.domain.transactionsstatistics.TransactionsStatistics;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TransactionsStatisticsTest {
  private static final double SUM = 1000.10;
  private static final double MAX = 200000.4944;
  private static final double MIN = 50.231;
  private static final int COUNT = 10;

  private static final TransactionsStatistics TRANSACTIONS_STATISTICS = new TransactionsStatistics(SUM, MAX, MIN, COUNT);

  @Test
  public void it_should_round_values_scaling_up() {
    assertThat(TRANSACTIONS_STATISTICS.getSum()).isEqualTo(SUM);
    assertThat(TRANSACTIONS_STATISTICS.getAvg()).isEqualTo(100.01);
    assertThat(TRANSACTIONS_STATISTICS.getMax()).isEqualTo(200000.49);
    assertThat(TRANSACTIONS_STATISTICS.getMin()).isEqualTo(50.23);
    assertThat(TRANSACTIONS_STATISTICS.getCount()).isEqualTo(COUNT);
  }

  @Test
  public void it_should_merge_sum() {
    assertThat(TRANSACTIONS_STATISTICS.merge(TRANSACTIONS_STATISTICS).getSum()).isEqualTo(SUM + SUM);
  }

  @Test
  public void it_should_merge_max() {
    double expectedMax = 10000000d;
    assertThat(
        TRANSACTIONS_STATISTICS.merge(new TransactionsStatistics(SUM, expectedMax, MIN, COUNT)).getMax()
    ).isEqualTo(expectedMax);
  }

  @Test
  public void it_should_merge_min() {
    double expectedMin = 23d;
    assertThat(
        TRANSACTIONS_STATISTICS.merge(new TransactionsStatistics(SUM, MAX, expectedMin, COUNT)).getMin()
    ).isEqualTo(expectedMin);
  }

  @Test
  public void it_should_merge_average() {
    assertThat(
        new TransactionsStatistics(12, MAX, MIN, COUNT)
            .merge(TRANSACTIONS_STATISTICS)
            .merge(TRANSACTIONS_STATISTICS)
            .getAvg()
    ).isEqualTo(67.07d);
  }

  @Test
  public void it_should_merge_count() {
    assertThat(
        TRANSACTIONS_STATISTICS
            .merge(TRANSACTIONS_STATISTICS)
            .merge(TRANSACTIONS_STATISTICS)
            .getCount()
    ).isEqualTo(30L);
  }

  @Test
  public void it_should_return_zero_when_there_are_no_transactions_so_far() {
    assertThat(new TransactionsStatistics(0d, 0d, 0d, 0L).getAvg()).isEqualTo(0);
  }
}
