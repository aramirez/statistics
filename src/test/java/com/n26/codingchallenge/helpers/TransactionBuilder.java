package com.n26.codingchallenge.helpers;

import com.n26.codingchallenge.domain.transaction.Transaction;

import java.time.Instant;

abstract public class TransactionBuilder {
  public static Transaction valid() {
    return new Transaction(12.33, Instant.now());
  }

  public static Transaction stale() {
    return new Transaction(12.33, Instant.now().minusSeconds(1000));
  }
}
