package com.n26.codingchallenge.infrastructure.http.controller;

import com.n26.codingchallenge.application.getstatistics.GetStatisticsResponse;
import com.n26.codingchallenge.application.getstatistics.GetStatisticsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatisticsControllerTest {

  @Mock
  private GetStatisticsService getStatisticsService;

  @Test
  public void it_should_return_statistics_given_by_the_use_case() {
    long count = 10;
    when(getStatisticsService.execute(any())).thenReturn(getGetStatisticsResponse(count));
    StatisticsController statisticsController = new StatisticsController(getStatisticsService);

    ResponseEntity<StatisticsDto> statistics = statisticsController.getStatistics();

    assertThat(statistics.getBody().getCount()).isEqualTo(count);
  }

  private GetStatisticsResponse getGetStatisticsResponse(long count) {
    return new GetStatisticsResponse(1000.00, 100.53, 200000.49, 50.23, count);
  }
}
