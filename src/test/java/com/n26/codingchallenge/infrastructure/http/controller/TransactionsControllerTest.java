package com.n26.codingchallenge.infrastructure.http.controller;

import com.n26.codingchallenge.application.createtransaction.CreateTransactionResponse;
import com.n26.codingchallenge.application.createtransaction.CreateTransactionService;
import com.n26.codingchallenge.application.deletetransactions.DeleteTransactionsRequest;
import com.n26.codingchallenge.application.deletetransactions.DeleteTransactionsResponse;
import com.n26.codingchallenge.application.deletetransactions.DeleteTransactionsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransactionsControllerTest {

  private TransactionsController transactionsController;

  @Mock
  private CreateTransactionService createTransactionService;

  @Mock
  private DeleteTransactionsService deleteTransactionsService;

  @Before
  public void setUp() {
    transactionsController = new TransactionsController(createTransactionService, deleteTransactionsService);
  }

  @Test
  public void it_should_create_a_transaction() {
    when(createTransactionService.execute(any())).thenReturn(CreateTransactionResponse.TRANSACTION_PERSISTED);

    double amount = 12.3343;
    Instant timestamp = Instant.now();
    TransactionDto transactionDto = new TransactionDto(amount, timestamp);
    transactionsController.createTransaction(transactionDto);

    verify(createTransactionService).execute(argThat(createTransactionRequest ->
        createTransactionRequest.getAmount() == amount
            && createTransactionRequest.getTimestamp().equals(timestamp)));
  }

  @Test
  public void it_should_call_delete_transactions_use_case() {
    when(deleteTransactionsService.execute(any())).thenReturn(new DeleteTransactionsResponse());

    transactionsController.deleteTransactions();

    verify(deleteTransactionsService).execute(any(DeleteTransactionsRequest.class));
  }

  @Test
  public void it_should_return_created_when_transaction_is_persisted() {
    when(createTransactionService.execute(any())).thenReturn(CreateTransactionResponse.TRANSACTION_PERSISTED);
    ResponseEntity<Void> response = transactionsController.createTransaction(new TransactionDto(11d, Instant.now()));

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
  }

  @Test
  public void it_should_return_no_content_when_transaction_is_discarded() {
    when(createTransactionService.execute(any())).thenReturn(CreateTransactionResponse.TRANSACTION_DISCARDED);
    ResponseEntity<Void> response = transactionsController.createTransaction(new TransactionDto(11d, Instant.now()));

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
  }

  @Test
  public void it_should_return_unprocessable_entity_when_transaction_is_in_the_future() {
    when(createTransactionService.execute(any())).thenReturn(CreateTransactionResponse.TRANSACTION_FAILED);
    ResponseEntity<Void> response = transactionsController.createTransaction(new TransactionDto(11d, Instant.now()));

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
  }
}
