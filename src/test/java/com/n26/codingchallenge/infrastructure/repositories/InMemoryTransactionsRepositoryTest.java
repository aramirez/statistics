package com.n26.codingchallenge.infrastructure.repositories;

import com.n26.codingchallenge.domain.transaction.Transaction;
import com.n26.codingchallenge.domain.transactionsstatistics.TransactionsStatistics;
import com.n26.codingchallenge.helpers.TransactionBuilder;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

public class InMemoryTransactionsRepositoryTest {

  private static final Instant NOW = Instant.now();
  private InMemoryTransactionsRepository repository;

  @Before
  public void setUp() {
    repository = new InMemoryTransactionsRepository();
  }

  @Test
  public void it_should_persist_a_single_transaction() {
    repository.persist(TransactionBuilder.valid());

    double amount = TransactionBuilder.valid().getAmount();

    assertThat(repository.getStatistics().getCount()).isEqualTo(1);
    assertThat(repository.getStatistics().getSum()).isEqualTo(amount);
    assertThat(repository.getStatistics().getAvg()).isEqualTo(amount);
    assertThat(repository.getStatistics().getMax()).isEqualTo(amount);
    assertThat(repository.getStatistics().getMin()).isEqualTo(amount);
  }

  @Test
  public void it_should_delete_elements() {
    repository.persist(new Transaction(2d, NOW));
    repository.persist(new Transaction(4d, NOW.minusSeconds(10)));
    repository.persist(new Transaction(8d, NOW.minusSeconds(20)));
    repository.persist(new Transaction(16d, NOW.minusSeconds(30)));
    repository.persist(new Transaction(32d, NOW.minusSeconds(40)));

    repository.deleteAll();

    assertThat(repository.getStatistics().getCount()).isEqualTo(0);
  }

  @Test
  public void it_should_summarise_statistics_for_multiple_transactions_in_different_times() {
    repository.persist(new Transaction(2d, NOW));
    repository.persist(new Transaction(4d, NOW.minusSeconds(10)));
    repository.persist(new Transaction(8d, NOW.minusSeconds(20)));
    repository.persist(new Transaction(16d,NOW.minusSeconds(30)));
    repository.persist(new Transaction(32d,NOW.minusSeconds(40)));

    TransactionsStatistics expectedStatistics = new TransactionsStatistics(62d, 0d, 0d, 5L);

    assertThat(repository.getStatistics().getCount()).isEqualTo(expectedStatistics.getCount());
    assertThat(repository.getStatistics().getSum()).isEqualTo(expectedStatistics.getSum());
    assertThat(repository.getStatistics().getMax()).isEqualTo(32d);
    assertThat(repository.getStatistics().getMin()).isEqualTo(2d);
  }

  @Test
  public void it_should_discard_stale_transactions_when_get_statistics() {
    repository.persist(TransactionBuilder.valid());
    repository.persist(TransactionBuilder.stale());

    assertThat(repository.getStatistics().getCount()).isEqualTo(1);
    assertThat(repository.getStatistics().getSum()).isEqualTo(TransactionBuilder.valid().getAmount());
    assertThat(repository.getStatistics().getMax()).isEqualTo(TransactionBuilder.valid().getAmount());
    assertThat(repository.getStatistics().getMin()).isEqualTo(TransactionBuilder.valid().getAmount());
  }

  @Test
  public void it_should_not_persist_stale_transactions() {
    repository.persist(TransactionBuilder.stale());

    assertThat(repository.getStatistics().getCount()).isEqualTo(0);
  }

  @Test
  public void it_should_summarise_statistics_for_multiple_transactions_in_the_same_time() {
    repository.persist(new Transaction(2d, NOW));
    repository.persist(new Transaction(4d, NOW));
    repository.persist(new Transaction(8d, NOW));
    repository.persist(new Transaction(16d,NOW));
    repository.persist(new Transaction(32d,NOW));

    TransactionsStatistics expectedStatistics = new TransactionsStatistics(62d, 0d, 0d, 5L);

    assertThat(repository.getStatistics().getCount()).isEqualTo(expectedStatistics.getCount());
    assertThat(repository.getStatistics().getSum()).isEqualTo(expectedStatistics.getSum());
    assertThat(repository.getStatistics().getMax()).isEqualTo(32d);
    assertThat(repository.getStatistics().getMin()).isEqualTo(2d);
  }
}
