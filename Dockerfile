FROM maven:3.5-jdk-8-slim as builder

WORKDIR /usr/src/app

# Copy pom and download dependencies before allows a better caching.
COPY pom.xml .
RUN mvn clean dependency:copy-dependencies

COPY checkstyle/ checkstyle/
COPY src src
COPY README.md .
RUN mvn package

FROM openjdk:8
COPY --from=0 /usr/src/app/target/coding-challenge-1.0.2-spring-boot.jar /usr/src/myapp/
WORKDIR /usr/src/myapp
CMD ["java", "-jar", "coding-challenge-1.0.2-spring-boot.jar"]

EXPOSE 8080
