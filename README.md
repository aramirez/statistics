# Transactions statistics

## How to run

### Launch locally:

```bash
$ mvn clean spring-boot:run
```

### Launch tests and checks

```bash
$ mvn clean test
```

*This launches automated tests and code analyser tools such as pmd, spotbugs and checkstyle.*

### Specs Verification

```bash
$ mvn clean integration-test; cat target/customReports/result.txt
```

*As describe in the challenge's specs, integration-test task MUST complete successfully.*

*Notice that I have omitted to add instructions on how to send requests and what the contract is because is that information is in the specifications in HackerRank. Otherwise, I would add some examples of how to send requests besides an API description using Swagger.*

### Docker

I have added a `Dockerfile` that allows to both compile the project and run it. It uses [Docker multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build/), so that the resulting Docker image does not need to have `mvn` but just `openjdk`.

**build and run using docker as follows**

```bash
$ docker build . -t statistics
$ docker run -p 8080:8080 statistics
```

## Resolution reasoning

The solution is built following [Hexagonal Architecture](http://alistair.cockburn.us/Hexagonal+architecture), which is one of the [clean architectures stated for Robert C. Martin](http://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) and that is gaining traction in the industry, mostly by those who try to implement domain-driven design.

The code is organised in three different packages, *infrastructure*, *application* and *domain* from outer to inner layers. Outer layers can talk directly with the next inner layer (this is, infrastructure can talk with application and application can talk with domain) but not the opposite.

The way that infrastructure and application layers talk is by using *data transfer objects* (DTO) for both application use cases in and out messages (called requests and responses). This leads to some drawbacks, for instance, the amount of request/responses classes is increased, some request/responses could be similar or even equal at all (but since they are not duplicated by concept (but by code) I would further keep it) and sometimes either request or responses can be unused and even empty, but I'd further keep them favouring the convention.

### Constant time and memory requirement (O(1))

The actual implementation is made using the **Repository pattern**. The interface is defined as `TransactionsRepository` and the only implementation is `InMemoryTransactionsRepository`.

In order to achieve **O(1)** when both creating a transaction and getting statistics the *AST* chosen is a `HashMap` (which has that time complexity for both put and get operations). The key for the map is the *timestamp* with a precision up to the second, also the key's hashcode is based on that timestamp to guarantee good distribution across the buckets.

As the specs file does not talk about keeping a record of the transactions, every time a transaction is created what really happens is the repository transform it to an instance of `TransactionsStatistics` and merge it with the current `TransactionsStatistics` found in that second (if any).

When getting statistics operation only has to merge statistics for those keys which are not stale yet.

Finally, it's worth mentioning that `InMemoryTransactionsRepository` uses a `ConcurrentHashMap` under the hood to guarantee safety with multiples threads.

### Rounding BigDecimals

I have decided to assume rounding as a domain requirement since I understood it reading the *specs document*. This means that **TransactionsStatistics** (inner value objects actually) class is in charge of that rounding to happen, also to have specific testing to ensure the right functioning.

The drawback of this implementation is that I had to create a custom *JsonSerializer* called **NumberSerializer** to ensure the serialization to JSON works as expected, because, Jackson by default, has a different behaviour than the expected.

## Quality assurance

Added some quality plugins in order to ensure minimum quality level besides find possible bugs before going productions. These plugins are:
- [PMD](https://pmd.github.io/): static code analyser.
- [Spotbugs](https://spotbugs.github.io/): static code analyser to look for bugs. It is the findbugs' successor. 
- [Checkstyle](http://checkstyle.sourceforge.net/): tool to ensure that the code follows an styling standard over time.
- [Jacoco](https://www.eclemma.org/jacoco/): checks for code coverage*.

\**Currently Spotbugs is not launched when validating since I faced some issues in HackerRank with the memory needed.*

\**On the time of writing this, code coverage is ~85%*

I have omitted adding contract tests because the `HttpJsonIT` (the checker provided) does something similar (although not the same). Otherwise I would have added contract testing using [Rest Assured](http://rest-assured.io/).

## Some other considerations

In despite the specs says:

> the solution must be at a quality level that you would be comfortable enough to put in production

There are some considerations before going to production that are out of the scope of this challenge.

### Observability

No service without logs nor metrics should go to production, otherwise we won't know what's going on that service.

Sending logs to an aggregator that eventually allows you to make use of them using either user interface and automated processes is a must. Some good options could be ELK stack, Splunk or even CloudWatch.

On the other hand, reporting metrics to the company's standard monitoring tools is also mandatory, this allows us to know how the system is performing and we can detect performance issues, locks and many other things. Also, good monitoring tools allow configuring alerts. Prometheus, Datadog, NewRelic, CloudWatchs are good examples of monitoring tools.

In distributed systems, using distributed tracing helps to see the whole request/response life-cycle and dependencies. Examples of this kind of tooling are OpenZipkin and Amazon's X-Ray.

### Catalogue and Services discovery

When doing either a micro-services architecture or SOA, keeping an up to date catalogue of services running in production is relevant since the number of services can grow to a huge number, so, having a centralised catalogue is really important.

On the other hand, with the increasing cloud-native applications deployed on cloud providers such of AWS, a **Service Discovery** is mandatory to keep track of what services are running on production and what the way to reach them is. The service discovery usually provides downstream services with some ways to discover upstream ones (e.g. ip/port, dns resolution...). So, the service must register itself to the service discovery once in production.

Some examples of services discovery are Netflix's Eureka and Hashicorp's Consul. On the other hand, both Etcd and Apache Zookeeper are sometimes used.

### Edge

Edge layer is a key part since it's the way services get exposed to the Internet. As this layer is in charge to apply some different rules, from security ones (e.g. firewall rules) to caching and content distribution, the service should be behind this layer, so once the service is running on production, the incoming traffic should come from the edge layer.
